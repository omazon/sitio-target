<?php

function send_mail($data,$template){
require'PHPMailer-master/PHPMailerAutoload.php';
$mail = new PHPMailer;
$mail->CharSet = 'UTF-8';
$mail->isSMTP();
$mail->SMTPDebug = 0;
$mail->Host = MAIL_HOST;
$mail->Port = MAIL_PORT;
$mail->IsHtml(true);
$mail->SMTPSecure = 'tls';
$mail->SMTPAuth = true;
$mail->Username = MAIL_USER;
$mail->Password = MAIL_PASS;
$mail->addReplyTo($data->email, $data->name);
$mail->setFrom($data->email, $data->name);
$mail->addAddress(MAIL_ADDRESS);
$mail->Subject = 'Mensaje del Sitio de Target';
$content = str_replace(
	array('{{name}}','{{phone}}','{{email}}','{{message}}'),
	array($data->name,$data->phone,$data->email,$data->message),
	file_get_contents($template)
);
$mail->msgHTML($content);
$mail->send();
}