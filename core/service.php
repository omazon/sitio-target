<?php
require 'config.php';

if(($_SERVER['HTTP_HOST']) == URL_SITE) {
	if($_SERVER['REQUEST_METHOD'] == 'POST'){
		$data = json_decode( file_get_contents("php://input" ));
		require 'database.php';
		$type = $data->type;
		if($type == 1){
			PostData::form_into($data,$db);
		}
	} else {
		header("Location: http://".URL_SITE);
	}
} else {
	header("Location: http://".URL_SITE);
};