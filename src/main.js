import Vue from 'vue'
import App from './App.vue'
// LIBRERIAS
import * as VueGoogleMaps from 'vue2-google-maps';
import VueScrollTo from 'vue-scrollto';
import VeeValidate,{Validator} from 'vee-validate';
import es from 'vee-validate/dist/locale/es';
import VueRecaptcha from 'vue-recaptcha';
window.axios = require ('axios');
// COMPONENTES
import header from '@/componentes/header.vue';
import trabajos from '@/componentes/trabajos.vue';
import clientes from '@/componentes/clientes.vue';
import mapa from '@/componentes/mapa.vue';
import formulario from '@/componentes/formulario.vue';
import banner from '@/componentes/banner.vue';
import pie from '@/componentes/pie.vue';

Vue.component('cabeza',header);
Vue.component('trabajos',trabajos);
Vue.component('clientes',clientes);
Vue.component('mapa',mapa);
Vue.component('formulario',formulario);
Vue.component('banner',banner);
Vue.component('pie',pie);
Vue.component('vue-recaptcha',VueRecaptcha);

Vue.use(VueGoogleMaps,{
   load:{
       key:'AIzaSyAzu8lKb98ieDd6Zr096ZzxGwo_ClEVMAU'
   }
});
Vue.use(VueScrollTo);
Validator.addLocale(es);
Vue.use(VeeValidate,{
    locale:'es'
});
new Vue({
  el: '#app',
  render: h => h(App)
});
new WOW().init();
window.onload = function () {
    document.querySelector('.loading').style.opacity = 0;
    document.querySelector('.loading').style.transition = 'opacity 05.s';
    document.querySelector('.loading + div').style.opacity = 1;
    document.querySelector('.loading + div').style.transition = 'opacity 1.5s';
};